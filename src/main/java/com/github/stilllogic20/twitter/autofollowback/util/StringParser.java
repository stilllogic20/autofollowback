package com.github.stilllogic20.twitter.autofollowback.util;

public final class StringParser {

  public static int parseToInt(String target) {
    if (target.isEmpty()) {
      return -1;
    }

    StringBuilder temp = new StringBuilder(target.length());
    for (char c : target.toCharArray()) {
      if ("0123456789".indexOf(c) == -1) {
        continue;
      }
      temp.append(c);
    }

    if (temp.toString().isEmpty()) {
      return -1;
    }
    return Integer.parseInt(temp.toString());
  }

}