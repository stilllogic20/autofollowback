package com.github.stilllogic20.twitter.autofollowback.event;

public interface HandlerInvoker<T> {

  void connectHandler(TwitterEventHandler<T> handler);

  void disconnectHandler(TwitterEventHandler<T> handler);

}
