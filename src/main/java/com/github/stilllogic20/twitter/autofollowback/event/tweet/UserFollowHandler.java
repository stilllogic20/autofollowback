package com.github.stilllogic20.twitter.autofollowback.event.tweet;

import com.github.stilllogic20.twitter.autofollowback.Users;
import com.github.stilllogic20.twitter.autofollowback.event.TwitterEventHandler;

import twitter4j.Twitter;
import twitter4j.TwitterException;

public final class UserFollowHandler implements TwitterEventHandler<Long> {

  public UserFollowHandler() {
  }

  @Override
  public void handle(Twitter twitter, Long userId) throws TwitterException {
    Users.followBack(twitter, userId);
  }

}
