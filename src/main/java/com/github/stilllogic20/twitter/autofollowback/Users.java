package com.github.stilllogic20.twitter.autofollowback;

import twitter4j.Twitter;
import twitter4j.TwitterException;

public final class Users {

  public static void followBack(Twitter twitter, long target) throws TwitterException {
    long id = twitter.verifyCredentials().getId();
    if (id == target) {
      return;
    }

    /*
     * lookupはRate Limitに引っかかるので非採用
     */

    twitter.createFriendship(target);
  }

}
