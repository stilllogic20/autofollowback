package com.github.stilllogic20.twitter.autofollowback.event.listeners;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.github.stilllogic20.twitter.autofollowback.event.HandlerInvoker;
import com.github.stilllogic20.twitter.autofollowback.event.TwitterEventHandler;
import com.google.common.base.Objects;
import com.google.common.collect.Sets;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.UserStreamAdapter;

public final class StreamFollowListener extends UserStreamAdapter implements HandlerInvoker<Long> {

  private final Logger logger;

  private final Twitter twitter;

  private final Set<TwitterEventHandler<Long>> handlers;

  public StreamFollowListener(Class<?> application, Twitter twitter) {
    this.twitter = twitter;
    handlers = Sets.newHashSet();
    logger = Logger.getLogger(application.getSimpleName());
  }

  @Override
  public void connectHandler(TwitterEventHandler<Long> handler) {
    handlers.add(handler);
  }

  @Override
  public void disconnectHandler(TwitterEventHandler<Long> handler) {
    handlers.remove(handler);
  }

  @Override
  public void onFollow(User source, User followedUser) {
    try {
      User me = twitter.verifyCredentials();
      if (Objects.equal(source, me) || !Objects.equal(followedUser, me)) {
        return;
      }

      for (TwitterEventHandler<Long> handler : handlers) {
        handler.handle(twitter, source.getId());
      }
    } catch (TwitterException checked) {
      logger.log(Level.INFO, "An exception was thrown", checked);
      return;
    }
  }

}
