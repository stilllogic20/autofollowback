package com.github.stilllogic20.twitter.autofollowback.event;

import twitter4j.Twitter;
import twitter4j.TwitterException;

public interface TwitterEventHandler<T> {

  void handle(Twitter twitter, T argument) throws TwitterException;

}
