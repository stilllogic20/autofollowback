package com.github.stilllogic20.twitter.autofollowback.event.tweet;

import java.util.regex.Pattern;

import com.github.stilllogic20.twitter.autofollowback.Users;
import com.github.stilllogic20.twitter.autofollowback.event.TwitterEventHandler;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public final class ReplyHandler implements TwitterEventHandler<Status> {

  public ReplyHandler() {}

  /** 反応するキーワード (正規表現) */
  private static final Pattern keyword = Pattern.compile("@.*(ふぉろー?ば(っく)?|フォロー?バ(ック)?).*");

  @Override
  public void handle(Twitter twitter, Status reply) throws TwitterException {
    if (keyword.matcher(reply.getText()).matches()) {
      Users.followBack(twitter, reply.getUser().getId());
    }
  }

}
