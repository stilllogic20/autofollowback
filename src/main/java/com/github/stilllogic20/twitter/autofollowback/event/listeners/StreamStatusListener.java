package com.github.stilllogic20.twitter.autofollowback.event.listeners;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.github.stilllogic20.twitter.autofollowback.event.HandlerInvoker;
import com.github.stilllogic20.twitter.autofollowback.event.TwitterEventHandler;
import com.google.common.collect.Sets;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.UserStreamAdapter;

public final class StreamStatusListener extends UserStreamAdapter implements HandlerInvoker<Status> {

  private final Logger logger;

  private final Twitter twitter;

  private final Set<TwitterEventHandler<Status>> handlers;

  public StreamStatusListener(Class<?> application, Twitter twitter) {
    this.twitter = twitter;
    handlers = Sets.newHashSet();
    logger = Logger.getLogger(application.getSimpleName());
  }

  @Override
  public void connectHandler(TwitterEventHandler<Status> handler) {
    handlers.add(handler);
  }

  @Override
  public void disconnectHandler(TwitterEventHandler<Status> handler) {
    handlers.remove(handler);
  }

  @Override
  public void onStatus(Status status) {
    try {
      String myScreenName = twitter.getScreenName();
      // System.out.println(status.getText());
      if (!status.getText().startsWith("@".concat(myScreenName))) {
        return;
      }

      for (TwitterEventHandler<Status> handler : handlers) {
        handler.handle(twitter, status);
      }
    } catch (TwitterException checked) {
      logger.log(Level.INFO, "An exception was thrown", checked);
      return;
    }
  }

}
