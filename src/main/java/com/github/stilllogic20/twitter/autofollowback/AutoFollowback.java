package com.github.stilllogic20.twitter.autofollowback;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.github.stilllogic20.twitter.autofollowback.event.listeners.StreamFollowListener;
import com.github.stilllogic20.twitter.autofollowback.event.listeners.StreamStatusListener;
import com.github.stilllogic20.twitter.autofollowback.event.tweet.ReplyHandler;
import com.github.stilllogic20.twitter.autofollowback.event.tweet.UserFollowHandler;
import com.github.stilllogic20.twitter.autofollowback.util.StringParser;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public final class AutoFollowback {

  public static void main(String[] args) throws TwitterException {
    boolean usePIN = TwitterInfo.USE_PIN_AUTHORIZE;
    final String consumerKey = TwitterInfo.CONSUMER_KEY;
    final String consumerSecret = TwitterInfo.CONSUMER_SECRET;
    final String accessToken = TwitterInfo.ACCESS_TOKEN;
    final String accessTokenSecret = TwitterInfo.ACCESS_TOKEN_SECRET;

    Configuration config = configure(usePIN, consumerKey, consumerSecret, accessToken, accessTokenSecret);
    Twitter twitter = getTwitter(config, usePIN);
    if (usePIN) {
      AccessToken known = twitter.getOAuthAccessToken();
      config = configure(false, consumerKey, consumerSecret, known.getToken(), known.getTokenSecret());
    }
    TwitterStream twitterStream = new TwitterStreamFactory(config).getInstance();
    StreamStatusListener statusListener = new StreamStatusListener(AutoFollowback.class, twitter);
    StreamFollowListener followListener = new StreamFollowListener(AutoFollowback.class, twitter);

    statusListener.connectHandler(new ReplyHandler());
    followListener.connectHandler(new UserFollowHandler());

    twitterStream.addListener(statusListener);
    twitterStream.addListener(followListener);

    System.gc();

    twitterStream.user();
  }

  private static Configuration configure(boolean usePIN,
      String consumerKey, String consumerSecret, String accessToken, String accessTokenSecret) {
    Configuration config = null;
    ConfigurationBuilder configBuilder = new ConfigurationBuilder()
        .setOAuthConsumerKey(consumerKey)
        .setOAuthConsumerSecret(consumerSecret);
    if (!usePIN) {
      configBuilder
          .setOAuthAccessToken(accessToken)
          .setOAuthAccessTokenSecret(accessTokenSecret);
    }
    config = configBuilder.build();
    return config;
  }

  private static Twitter getTwitter(Configuration config, boolean usePIN) {
    TwitterFactory factory = new TwitterFactory(config);
    Twitter twitter = factory.getInstance();
    if (usePIN) {
      try {
        RequestToken requestToken = twitter.getOAuthRequestToken();
        Runtime.getRuntime().exec(System.getenv("comspec") + " /c start " + requestToken.getAuthorizationURL());

        String pin = "";
        int rawPin = -1;
        BufferedReader pinReader = new BufferedReader(new InputStreamReader(System.in));
        do {
          System.out.print("PIN Code: ");
          rawPin = StringParser.parseToInt(pinReader.readLine());
          pin = String.valueOf(rawPin);
        } while (rawPin == -1);

        AccessToken token = twitter.getOAuthAccessToken(requestToken, pin);
        System.out.println("AccessToken: " + token.getToken());
        System.out.println("AccessToken Secret: " + token.getTokenSecret());

      } catch (TwitterException exception) {
        throw new RuntimeException(exception);
      } catch (IOException exception) {
        throw new InternalError(exception);
      }
    }
    return twitter;
  }

}
